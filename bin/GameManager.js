// =============================================================== \\
// ================== Server-side: GameManager =================== \\
// =============================================================== \\
// == Manages all the players who plays the game (so it manages == \\
// == also all the games). ======================================= \\


var Player = (function () {

    function Player (settings) {
        this.name = settings.name ? settings.name : 'Anonymous';
        this.isHost = false;
        this.isReady = false;
        this.socket = settings.socket ? settings.socket : null;
        this.opponent = null;
        this.game = '';
    }

    Player.prototype.toggleIsHost = function () {
        this.isHost = !this.isHost;
    };

    Player.prototype.toggleIsReady = function () {
        this.isReady = !this.isReady;
    };

    return Player;
})();


var Game = (function () {
    function Game (settings) {
        this.name = settings.gameName ? settings.gameName : "Anonymous game";
        this.player1 = settings.player1 ? settings.player1 : null;
        this.player2 = settings.player2 ? settings.player2 : null;
    }

    Game.prototype.isFull = function () {
        return !!this.player2;
    };

    return Game;

})();

/** TODO: Consider more persistent storage. */
var Players = [],
    games,
    io;

var GameManager = (function () {


    /**
    * This function is called whenever a client connects to the server. It handles all
    * communication between clients.
    *
    *
    * @param(settings) io The socket.io library
    * @param(settings) socket The socket for the newly connected client
    * @param(settings) playername The player name
    */
    function GameManager (settings) {
        this.io = settings.io;
        io = settings.io;
        games = settings.Games;
        this.socket = settings.socket;
        this.playername = settings.playername;
    }


    GameManager.prototype.init = function () {
        var self = this;
        // Save the player
        Players[this.socket.id] = new Player({name: this.playername, socket: this.socket});

        var size = 0;
        Players.forEach(function (player) {
            size++;
            console.log('inside forEach');
        });



        this.socket.on('hostGame', hostGame);
        this.socket.on('awaitingReadyGame', awaitingReadyGame);

        // /** If the player acts as a CLIENT. */
        this.socket.on('joinGame', joinGame);

        // * To all players, whether host or client.
        this.socket.on('gameMovement', gameMovement);
        this.socket.on('gameShooting', gameShooting);
        this.socket.on('changeGameStatus', changeGameStatus);
        this.socket.on('endGame', endGame);

        // this.socket.on('removeEnemy', removeEnemy);
        // this.socket.on('moveEnemies', moveEnemies);

        // /** Show the newly joined player all available games. */
        this.socket.on('showGames', showAllGames);
        // showAllGames.call(this.socket);
    };


    /************** HELPER FUNCTIONS **************/

    function gameNameAlreadyExists(gameName, socket) {

        var gameRoom = socket.join[gameName];
        if (gameRoom !== undefined) return true;
        else return false;
    }


    function showAllGames() {
        var socket = this;
        console.log('showing games: ' + games.length);
        games.forEach(function (game) {
            console.log(game);
            if (!game.isFull()) {
                socket.emit('hostedGame', {
                    game: game.name,
                    player: game.player1.name
                });
            }
        });
    }


/*************** HOST FUNCTIONS ***************/

function hostGame(data) {
    var gameName = data.gameName;

    var socket = this;

    /** If the game name already exists make the client try another name. */
    if (gameNameAlreadyExists(gameName, socket)) {
        socket.emit('error', { msg: "Game name already exists! Try another." });
    }
    /** Else setup the game and report to all clients that a new game is available. */
    else {
        var me = Players[socket.id];
        games.push(new Game({gameName: gameName, player1: me}));
        me.toggleIsHost();
        me.game = gameName;
        console.log("'" + me.name + "' wants to host a game called '" + gameName + "'.");
        socket.broadcast.emit('hostedGame', {game: gameName, player: me.name});
        socket.join(gameName);
    }
}

function awaitingReadyGame(data) {
    var socket = this;
    var me = Players[socket.id];
    console.log("'" + me.name + "' is ready to play!");
    console.log("isHost: " + me.isHost);
    me.isReady = true;
    var clients = Object.keys(io.nsps['/'].adapter.rooms[me.game]);
    // var clients = io.sockets.clients(me.get("game"));
    console.log(clients);
    if (me.isHost) {
        me.opponent = clients[1];
    }
    for (var i = clients.length - 1; i >= 0; i--) {
        console.log(clients[i].id);
        console.log(Players);
        var tmpPlayer = Players[clients[i]];
        console.log("'" + tmpPlayer.name + "' is ready: " + tmpPlayer.isReady);
        if (!tmpPlayer.isReady) {
            return;
        }
    }
    /** Message the players they may start to play. */
    io.to(me.game).emit('playGame', {go: '!'});
    // io.sockets.in(me.get("game")).emit('playGame', { go: "!"});
}

/************** CLIENT FUNCTIONS **************/

function joinGame (data) {
    var socket = this;
    /** Trying to find the game. */
    var room = io.sockets.adapter.rooms[data.game];
    // var room = socket.rooms[data.game];
    var theGame = null;
    games.forEach(function (game) {
        console.log(game.name);
        if (game.name === data.game) {
            theGame = game;
        }
    });
  console.log('theGame: ' + theGame);
  if (theGame === null) {
    // Error: No such game exists.
    console.log("Error: No such game exists.");
    socket.emit('joinGame', {success: false, error: "The game no longer exists."});
    return;
  }
  // If the game is full.
  if (theGame.isFull()) {
    // Error: Game is full.
    console.log("Error: Game is full.");
    socket.emit('joinGame', {success: false, error: "The game is full."});
    return;
  }

  /** If the game exists. */
  if (room !== undefined) {

    /** Find other clients connected to the game. */
    // console.log(io.nsps['/'].adapter.rooms[data.game]);
    // console.log(Object.keys(io.nsps['/'].adapter.rooms[data.game]).length);
    // console.dir(io.sockets);
    // console.dir(io.sockets.adapter);
    // console.dir(io.sockets.rooms);
    // console.dir(io.sockets.clients);
    var clients = Object.keys(io.nsps['/'].adapter.rooms[data.game]);
    // var clients = io.sockets.clients(data.game);

    /** There can only be one host and one client. */
    if (clients.length > 1) {
        socket.emit('error', { msg: "Game is full!" });
        return;
    }
    else {
        var me = Players[socket.id];
        me.game = data.game;
        /** Join the game. */
        socket.join(data.game);
        /** Set the opponent. */
        me.opponent = clients[0];
        console.log('CLIENTS --------------');
        // console.dir(clients);
        console.log(clients[0]);
        // console.log(me);
        // Fetch opponent info.
        /** Report to both host and client that they found each other <3. */
        // io.sockets.in(data.game).emit('fullGame', {name: me.get("name"), opponent: opponent.get("name")});
        // io.sockets.in(data.game).emit('joinGame', {me: socket.id, opponent: me.opponent.id, success: true});

        io.to(data.game).emit('joinGame', {me: socket.id, opponent: me.opponent.id, success: true});
        // Report to all other players that the game is no longer available.
        io.sockets.emit('removeGame', {game: data.game});
    }
  }
  /** If not; give an error to the client. */
  else {
    socket.emit('error', { message: "This game does not any longer exist." });
  }
}

function endGame (data) {
    var me = Players[this.id],
        opponent = Players[me.opponent];

    // Tell the two players who the victor is.
    if (data.victory) {
        io.to(me.game).emit('endGame', me.name);
    }
    else {
        io.to(me.game).emit('endGame', opponent.name);
    }
}


/************* GENERAL FUNCTIONS **************/

function gameMovement(data) {
    // console.log('GameMovement: ' + this.id);
    var me = Players[this.id];
    // console.log(me);
    // me.opponent.socket.emit('gameMovement', data);
    io.to(me.opponent).emit('gameMovement', data);
    // io.to(socketid).emit('message', 'for your eyes only');
}

function gameShooting(data) {
    var me = Players[this.id];
    // me.opponent.socket.emit('gameShooting', data);
    io.to(me.opponent).emit('gameShooting', data);
}

/**
 * Change the game status. Currently only paused and active
 * are the available game statuses.
 */
function changeGameStatus(data) {
    console.log("Received status change to : " + data.gameStatus);
    var me = Players[this.id];
    var game = me.game;
    if (data.gameStatus === 'paused') {
        // io.sockets.in(game).emit('pauseGame');
        io.to(game).emit('pauseGame');
        console.log('paused!');
    }
    else if (data.gameStatus === 'active') {
        io.to(game).emit('resumeGame');
        // io.sockets.in(game).emit('resumeGame');
        console.log('active!');
    }
    else {
        console.log("Not able to determine status");
    }

}

function removeEnemy(data) {
    var me = Players[this.id];
    me.opponent.emit('removeEnemy', data);
}

function moveEnemies(data) {
    var me = Players[this.id];
    me.opponent.emit('moveEnemies', data);
}

    return GameManager;


})();

module.exports = GameManager;