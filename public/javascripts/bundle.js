(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/** Models */
var Block = require('./models/Block'),
	Enemy = require('./models/Enemy'),
	Missile = require('./models/Missile'),
	Player = require('./models/Player'),
	GameInstance = require('./models/GameInstance'),
/** Controllers */
	PlayerController = require('./controllers/PlayerController');


var Multivaders = {

    playerInstance: null,
    opponentInstance: null,
    socket: io.connect(window.location.hostname + ':' + window.location.port),
    isHosting: false,
    isReady : false,

    init: function () {
        this.step1 = document.getElementById('step1');
        this.step2 = document.getElementById('step2');
        this.step3 = document.getElementById('step3');
        this.bodyEl = document.getElementsByTagName('body')[0];
        this.playBtn = document.getElementById('readyToPlay');
        this.pauseBtn = document.getElementById('pauseGame');
        this.resumeBtn = document.getElementById('resumeGame');
        this.usernameInputEl = document.getElementById('nameinput');
        this.hostInputEl = document.getElementById('hostinput');

        this.listenToInput();
        this.listenToSocket();

        this.socket.emit('showGames');
    },

    // Start setting up the game.
    startGame: function () {
        // Create an instance for the player and the opponent.
        var playerInstance = new GameInstance({person: 'player_me', socket: this.socket, multivaders: this}),
            opponentInstance = new GameInstance({person: 'player_opponent', socket: this.socket, multivaders: this});

        // Initiate these instances.
        playerInstance.init();
        opponentInstance.init();


        // Create a new player and opponent and give them the instance where they can play.
        var player = new Player({
            instance: playerInstance,
            svg: playerInstance.svg,
            width: 50,
            height: 25,
            x: (playerInstance.width/2)-25,
            y: playerInstance.height-40
        }),
        opponent = new Player({
            instance: opponentInstance,
            svg: opponentInstance.svg,
            width: 50,
            height: 25,
            x: (opponentInstance.width/2)-25,
            y: playerInstance.height-40
        });

        // Initiate the players.
        player.init();
        opponent.init();


        // Populate the instances.
        playerInstance.Player = player;
        playerInstance.createEnemies(1, 1, 7);
        playerInstance.createEnemies(2, 2, 7);
        playerInstance.createEnemies(3, 3, 7);
        playerInstance.createBlocks(3, 3, 6);

        opponentInstance.Player = opponent;
        opponentInstance.createEnemies(1, 1, 7);
        opponentInstance.createEnemies(2, 2, 7);
        opponentInstance.createEnemies(3, 3, 7);

        opponentInstance.createBlocks(3, 3, 6);

        // var Blocks = playerInstance.Blocks;

        // We only need one controller that is for the player. We will receive movement stuff from
        // the opponent through the server.
        var playerController = new PlayerController({server: Multivaders.socket, player: player, opponent: opponent});
        playerController.init();

        this.playerInstance = playerInstance;
        this.opponentInstance = opponentInstance;

        // Finally we can start the game :)!
        playerInstance.startGame();
        opponentInstance.startGame();
    },


    // Listens to input from the client.
    listenToInput: function () {
        Multivaders.usernameInputEl.addEventListener('keydown', Multivaders.handleInputUsername, false);
        Multivaders.hostInputEl.addEventListener('keydown', Multivaders.handleInputHost, false);
        Multivaders.resumeBtn.addEventListener('click', Multivaders.handleResume, false);
        Multivaders.pauseBtn.addEventListener('click', Multivaders.handlePause, false);
        Multivaders.playBtn.addEventListener('click', Multivaders.handlePlay, false);
        Multivaders.bodyEl.addEventListener('click', Multivaders.handleClicksOnDynamicElements, false);
    },


    // Listens to messages sent by the server.
    listenToSocket: function () {
        // Relevant to pre-game stuff. 
        Multivaders.socket.on('hostedGame', Multivaders.hostedGame);
        Multivaders.socket.on('joinGame', Multivaders.joinGame);
        Multivaders.socket.on('playGame', Multivaders.playGame);

        // Relevant to when the game has started.
        Multivaders.socket.on('endGame', Multivaders.endGame);
        Multivaders.socket.on('pauseGame', Multivaders.pauseGame);
        Multivaders.socket.on('resumeGame', Multivaders.resumeGame);
    },

    // Add the newly hosted game to the list of games.
    hostedGame: function (data) {
        var newGame = document.createElement('tr');
        newGame.innerHTML = '<td>' + data.game + '</td>' +
        '<td>' + data.player + '</td>' +
        '<td><button class="joinGame" data-game="' + data.game + '">You join now!</button></td>';

        var gamesList = document.getElementById('gameslist');
        gamesList.appendChild(newGame);
    },

    // Join a game hosted by another player.
    joinGame: function (data) {
        if (data.success) {
            // Remove the host/join games functionality
            Multivaders.step2.style.display = 'none';
            Multivaders.step3.style.display = 'block';
        }
        else {
            // Failure in joining game. Host may have dismissed game or another player joined
            // before you.
            // TODO: Provide message to inform the user about this.
        }
    },

    // The server says it's time to play!
    playGame: function () {
        var gameArea = document.getElementsByClassName('gameArea')[0];

        // Transition into the game (finally!).
        Multivaders.step3.style.display = 'none';
        gameArea.style.display = 'block';

        Multivaders.startGame();
    },

    // Received a message from the server that the game has ended.
    // That means that we've won/lost if the opponent lost/won.
    endGame: function (winner) {
        Multivaders.playerInstance.endGame(winner);
    },

    // Pause the game (for both the player and the opponent).
    pauseGame: function () {
        Multivaders.playerInstance.pauseGame();
        Multivaders.opponentInstance.pauseGame();
    },

    // Resume the game (for both the player and the opponent).
    resumeGame: function () {
        Multivaders.playerInstance.resumeGame();
        Multivaders.opponentInstance.resumeGame();
    },

    // Register the username of the player.
    handleInputUsername: function (e) {
        var keyEvent = e.keyCode ? e.keyCode : e.which;
        // If the user pressed 'enter'.
        if (keyEvent === 13) {
            Multivaders.socket.emit('joinReq', { name: this.value });
            Multivaders.hostInputEl.readOnly = false;
            Multivaders.hostInputEl.placeholder = 'you name game...';
            this.readOnly = true;

            Multivaders.step2.classList.toggle('inactive');
            Multivaders.step1.classList.toggle('inactive');
        }
    },

    // If the user wants to host a game.
    handleInputHost: function (e) {
        var keyEvent = e.keyCode ? e.keyCode : e.which;
        // If the user pressed 'enter' and is not already hosting a game.
        if (keyEvent === 13 && !Multivaders.isHosting) {
            Multivaders.socket.emit('hostGame', { gameName: this.value });
            Multivaders.isHosting = true;

            var gameMsg = document.querySelector('#step2 .gameMsg');
            gameMsg.innerText = "Lonely a little now, but friend come soon!!";
        }
    },

    // Inform the server that the user wants to resume the game.
    handleResume: function (e) {
        e.preventDefault();
        Multivaders.socket.emit('changeGameStatus', { gameStatus: 'active' });
    },

    // Inform the server that the user wants to pause the game.
    handlePause: function (e) {
        e.preventDefault();
        Multivaders.socket.emit('changeGameStatus', { gameStatus: 'paused' });
    },

    // Inform the server that you are ready to play!
    handlePlay: function (e) {
        e.preventDefault();
        if (this.isReady) {
            // TODO: Output something like "U WAIT!! FREND NOT TO READY!!"
            return;
        }
        isReady = true;
        // Tell the server that you are ready.
        Multivaders.socket.emit('awaitingReadyGame', { ready: true });
        // Inform the user that we're waiting for his "friend".
        document.querySelector('#step3 .gameMsg').innerText = 'Waiting for friend to ready!';
    },

    // When someone hosts a game a new element is added. This functions such dynamic elements.
    handleClicksOnDynamicElements: function (e) {
        if (e.target.tagName.toLowerCase() === 'button') {
            e.preventDefault();
            var target = e.target;
            if (target.classList.contains('joinGame')) {
                server.emit('joinGame', { game: target.dataset.game });
            }
        }
    }

};

// Start the application.
Multivaders.init();


},{"./controllers/PlayerController":2,"./models/Block":3,"./models/Enemy":4,"./models/GameInstance":5,"./models/Missile":6,"./models/Player":7}],2:[function(require,module,exports){
// =============================================================== \\
// ================== In-Game Controller Model =================== \\
// =============================================================== \\

var PlayerController = (function () {
    function PlayerController (settings) {
        this.server = settings.server;
        this.multivader = settings.multivader;
        this.player = settings.player;
        this.opponent = settings.opponent;
        this.gameActive = true;
    }


    PlayerController.prototype.init = function() {
        keyEvents(this);
        opponentEvents(this);
    };

    var keyEvents = function (self) {
        if (self.gameActive) {
            // window.onkeydown = function(event) {
            window.addEventListener('keydown', function (event) {

                var keyEvent = event.keyCode ? event.keyCode : event.which,
                    player = self.player,
                    server = self.server;
                // Left
                if (keyEvent == 37) {
                    event.preventDefault();
                    player.move((player.x-10), player.y);
                    server.emit('gameMovement', {coordX: player.x-10});
                }
                // Right
                else if (keyEvent == 39) {
                    event.preventDefault();
                    player.move((player.x+10), player.y);
                    server.emit('gameMovement', {coordX: player.x+10});
                }
                // Up (Shoot)
                else if (keyEvent == 38) {
                    event.preventDefault();
                    player.shoot(player.x, player.y);
                    server.emit('gameShooting', {coordX: player.x, coordY: player.y});
                }
            // };
            }, false);
        }
        else {
            console.log('Game is paused; keys are disabled');
        }
    };

    var opponentEvents = function (self) {

        // var controller = this;

        self.server.on('gameMovement', function(data) {
            // Spaceships can only move along the x-axis. The y-coordinate remains the same.
            self.opponent.move(data.coordX, self.opponent.y);
        });

        self.server.on('gameShooting', function(data) {
            self.opponent.shoot(data.coordX, data.coordY);
        });

        self.server.on('pauseGame', function(data) {
            // if (gameStatus === 1) { return; }
            // controller.set({ gameStatus: 1 });
            self.gameActive = false;
            // self.multivaders.pauseGame();
        });

        self.server.on('resumeGame', function(data) {
            // if (gameStatus === 0) { return; }
            // controller.set({ gameStatus: 0 });
            self.gameActive = true;
            // self.multivaders.runGame();
        });
    };

    return PlayerController;

})();

module.exports = PlayerController;

// module.exports = PlayerController;


// module.exports = Backbone.Model.extend({
//     defaults: function() {
//         return {
//             server: null,
//             multivader: null,
//             player: null,
//             opponent: null,
//             gameActive: false,
//             gameStatus: 0
//         };
//     },

//     init: function() {
//         this.keyEvents();
//         this.clickEvents();
//         this.opponentEvents();
//     },

//     keyEvents: function() {

//         var server = this.get("server");
//         var player = this.get("player");
//         var psvg = player.get("ship");
//         var controller = this;
//         d3.select("body").on('keydown', function() {
//             if (!Boolean(controller.get("gameActive"))) {
//                 console.log("Game paused: keys disabled.");
//                 return;
//             }
//             var keyEvent = d3.event.keyCode;
//             // Left
//             if (keyEvent == 65) {
//                 var curX = player.get("x");
//                 var curY = player.get("y");
//                 player.move((curX-10), curY);
//                 server.emit('gameMovement', {coordX: curX-10});
//             }
//             // Right
//             else if (keyEvent == 68) {
//                 var curX = player.get("x");
//                 var curY = player.get("y");
//                 player.move((curX+10), curY);
//                 server.emit('gameMovement', {coordX: curX-10});
//             }
//             // Up (Shoot)
//             else if (keyEvent == 87) {
//                 var curX = Number(player.get("x"));
//                 var curY = Number(player.get("y"));
//                 console.log("Up: " + curX + "/" + curY);
//                 player.shoot(curX, curY);
//                 server.emit('gameShooting', {coordX: curX, coordY: curY});
//             }
//         });
//     },

//     clickEvents: function() {
//         var server = this.get("server");
//         $('.gameButtons').on('click', '#pauseGame', function(e) {
//             e.preventDefault();
//             server.emit('changeGameStatus', { gameStatus: 'paused' });
//         });

//         $('.gameButtons').on('click', '#resumeGame', function(e) {
//             e.preventDefault();
//             server.emit('changeGameStatus', { gameStatus: 'active' });
//         });
//     },

//     opponentEvents: function() {
//         var server = this.get("server");
//         var opponent = this.get("opponent");
//         var oship = opponent.get("ship");
//         var gameStatus = this.get("gameStatus");
//         var controller = this;
//         var Multivaders = this.get("multivader");

//         server.on('gameMovement', function(data) {
//             // Spaceships can only move along the x-axis. The y-coordinate remains the same.
//             opponent.move(data.coordX, opponent.get("y"));
//         });

//         server.on('gameShooting', function(data) {
//             opponent.shoot(data.coordX, data.coordY);
//         });

//         server.on('pauseGame', function(data) {
//             if (gameStatus == 1) { return; }
//             controller.set({ gameStatus: 1 });
//             Multivaders.pauseGame();
//         });

//         server.on('resumeGame', function(data) {
//             if (gameStatus == 0) { return; }
//             controller.set({ gameStatus: 0 });
//             Multivaders.runGame();
//         });
//     }

// });
},{}],3:[function(require,module,exports){
var Block = (function () {
    function Block (settings) {
        this.svg = settings.svg ? settings.svg : null;
        this.width = 20;
        this.height = 20;
        this.block = settings.block ? settings.block : null;
        this.x = settings.x ? settings.x : null;
        this.y = settings.y ? settings.y : null;
    }

    // ---------------------------- \\
    //      Public method(s)        \\
    // ---------------------------- \\
    Block.prototype.init = function () {
        buildGraphics(this);
    };

    // Checks if a missile is within the space of the block -> a collsion has occurred
    Block.prototype.checkForCollision = function (data) {
        // Fetch the boundary of the missile and the block.
        var mLeftX = data.leftX,
            mRightX = data.rightX,
            mTopY = data.topY,
            leftX = this.x,
            rightX = this.x + this.width,
            topY = this.y,
            bottomY = this.y + this.height;

        // See if the missile is within the same space as the block.
        var mRightXHits = (leftX < mRightX) && (mRightX < rightX),
            mLeftXHits = (leftX < mLeftX) && (mLeftX < rightX),
            mTopYHits = (topY < mTopY) && (mTopY < bottomY);

        // The block is destroyed!
        if (mTopYHits && (mRightXHits || mLeftXHits)) {
            this.block.remove();
            return true;
        }
        // The block dodged a missile(/bullet)!
        return false;
    };

    // ---------------------------- \\
    //      Private method(s)       \\
    // ---------------------------- \\
    var buildGraphics = function (self) {
        self.block = self.svg.append('rect')
        .attr('x', self.x)
        .attr('y', self.y)
        .attr('width', self.width)
        .attr('height', self.height)
        .style('fill', '#1f77b4');
    };

    return Block;
})();

module.exports = Block;
},{}],4:[function(require,module,exports){
var Missile = require('./Missile.js');

var Enemy = (function () {
    function Enemy (settings) {
        this.instance = settings.instance;
        this.svg = settings.svg ? settings.svg : null;
        this.width = settings.width ? settings.width : 30;
        this.height = settings.height ? settings.height : 30;
        this.ship = settings.ship ? settings.ship : null;
        this.x = settings.x ? settings.x : 0;
        this.y = settings.y ? settings.y : 0;
        this.isDestroyed = false;
        this.imageOne = settings.imageOne ? settings.imageOne : '/images/invader1.png';
        this.imageTwo = settings.imageTwo ? settings.imageTwo : this.imageOne;
        this.worth = settings.worth ? settings.worth : 0;
        this.movementSpeed = settings.movementSpeed ? settings.movementSpeed : 0;
        this.face = 0;
    }

    // ---------------------------- \\
    //      Public method(s)        \\
    // ---------------------------- \\

    Enemy.prototype.buildGraphics = function () {
        buildGraphics(this);
    };

    Enemy.prototype.move = function (newX, newY) {
        changeImage(this);
        this.x = newX;
        this.y = newY;
        this.ship.transition().duration(this.movementSpeed)
        .attr('x', this.x)
        .attr('y', this.y);
    };

    Enemy.prototype.checkForCollision = function (data) {
        // Fetch the boundary of the missile and the invader.
        var mLeftX = data.leftX,
            mRightX = data.rightX,
            mTopY = data.topY,
            leftX = this.x,
            rightX = this.x + this.width,
            topY = this.y,
            bottomY = this.y + this.height;

        // See if the missile is within the same space as the invader ship.
        var mRightXHits = (leftX < mRightX) && (mRightX < rightX),
            mLeftXHits = (leftX < mLeftX) && (mLeftX < rightX),
            mTopYHits = (topY < mTopY) && (mTopY < bottomY);

        // The invader ship is destroyed!
        if (mTopYHits && (mRightXHits || mLeftXHits)) {
            this.isDestroyed = true;
            this.ship.remove();
            this.instance.invaderDestroyed(this);
            return true;
        }
        // The invader dodged a missile(/bullet)!
        return false;
    };

    Enemy.prototype.shoot = function(self) {
       var tmpMissile = new Missile({
            instance: self,
            server: null,
            player: this,
            svg: this.svg,
            x: this.x + (this.width/2),
            y: this.y + (this.height),
            speed: -10
       });
       tmpMissile.init();
       tmpMissile.shoot();
    };

    // ---------------------------- \\
    //      Private method(s)       \\
    // ---------------------------- \\

    var buildGraphics = function (self) {
        self.ship = self.svg.append('image')
        .attr('x', self.x)
        .attr('y', self.y)
        .attr('width', self.width)
        .attr('height', self.height)
        .attr('xlink:href', self.imageOne);
    },
    changeImage = function (self) {
        if (self.face === 0) {
            self.ship.attr('xlink:href', self.imageTwo);
            self.face = 1;
        }
        else {
            self.ship.attr('xlink:href', self.imageOne);
            self.face = 0;
        }

    };

    return Enemy;
})();

module.exports = Enemy;
},{"./Missile.js":6}],5:[function(require,module,exports){


var Enemy = require('./Enemy');
var Block = require('./Block');

var DIRECTION_LEFT = -1,
    DIRECTION_RIGHT = 1;



var GameInstance = (function () {
    function GameInstance (settings) {
        this.Blocks = [];
        this.Enemies = [];
        this.Player = settings.Player ? settings.Player : null;
        this.multivaders = settings.multivaders;
        this.socket = settings.socket;

        this.direction = DIRECTION_RIGHT;
        this.width = settings.width ? settings.width : 500;
        this.height = settings.height ? settings.height : 700;
        this.movementDistance = settings.movementDistance ? settings.movementDistance : 30;
        this.difficulty = settings.difficulty ? settings.difficulty : 2;
        this.person = settings.person ? settings.person : 'me';
        this.svg = null;
        this.gameSpeed = 1500;
        this.gameTimer = null;
        this.bombardmentTimer = null;
        this.invadersDestroyedCounter = 0;
        this.invadersDestroyedMarker = [5, 10];
    }

    // ---------------------------- \\
    //      Public method(s)        \\
    // ---------------------------- \\

    GameInstance.prototype.init = function() {
        buildGraphics(this);

    };

    GameInstance.prototype.createEnemies = function (enemyNr, enemyLine, enemyAmount) {
        var enemyIndex = enemyLine-1;
        this.Enemies[enemyIndex] = this.Enemies[enemyIndex] ? this.Enemies[enemyIndex] : [];
        for (var i = 0; i < enemyAmount; i++) {
            var en = new Enemy({
                instance: this,
                svg: this.svg,
                width: 30,
                height: 30,
                x: 10 + (i*40),
                y:10 + (enemyLine * this.movementDistance),
                imageOne: '/images/invader' + enemyNr + '.png',
                imageTwo: '/images/invader' + enemyNr + '_2.png'
            });
            en.buildGraphics();
            this.Enemies[enemyIndex].push(en);
        }
    };

    GameInstance.prototype.createBlocks = function(rowAmount, colAmount, blockAmount) {
        for (var h = 0; h < blockAmount; h++) {
            for (var i = 0; i < rowAmount; i++) {
                for (var j = 0; j < colAmount; j++) {
                    var bl = new Block({svg: this.svg, x: 10 + (j*21) + (h*80), y:550 + (21*i) });
                    bl.init();
                    this.Blocks.push(bl);
                }
            }
        }
    };


    GameInstance.prototype.invaderDestroyed = function(enemy) {
        this.invadersDestroyedCounter = this.invadersDestroyedCounter + 1;
        // console.log('Destroyed this enemy: ' + enemy);
        // Remove the invader from our invader list.
        removeEnemy(this, enemy);
        // console.log('this.invadersDestroyedMarker: ' + this.invadersDestroyedMarker[0]);
        // If a certain amount of invaders have been destroyed, increase the speed.
        if (this.invadersDestroyedMarker[0] !== undefined && this.invadersDestroyedCounter >= this.invadersDestroyedMarker[0]) {
            // console.log('after: this.invadersDestroyedMarker: ' + this.invadersDestroyedMarker[0]);
            this.invadersDestroyedMarker.shift();
            this.gameSpeed = this.gameSpeed - 500;
            // console.log('SETTING GAMESPEED TO: ' + this.gameSpeed);
            window.clearInterval(this.gameTimer);
            window.clearInterval(this.bombardmentTimer);
            var self = this;
            window.setTimeout(function () {
                startInvadersMovement(self);
                startInvadersBombardment(self);
            }, 10);

            // console.log('MAKING GAME FASTER!!!!');
        }
        else if (this.invadersDestroyedMarker[0] === undefined)  {
            // console.log('InvaderDestroyed === undefined');
            // window.clearInterval(this.gameTimer);
            // window.clearInterval(this.bombardmentTimer);
        }

    };

    GameInstance.prototype.startGame = function () {
        startInvadersMovement(this);
        startInvadersBombardment(this);
    };    

    GameInstance.prototype.endGame = function (winner) {
        stopInvaders(this);
        this.showEndGameBoard(winner);
    }

    GameInstance.prototype.pauseGame = function () {
        stopInvaders(this);
    };

    GameInstance.prototype.resumeGame = function () {
        startInvadersMovement(this);
        startInvadersBombardment(this);
    };

    GameInstance.prototype.reportEndGame = function (victory) {
        stopInvaders(this);
        // Tell the server that the opponent won/lost if the player lost/won.
        this.socket.emit('endGame', { victory: !!victory });
        // Show the finished-game-view.
        // this.multivaders.showEndGameBoard(victory);
    };

    GameInstance.prototype.showEndGameBoard = function (winner) {
        var endboard = document.getElementsByClassName('endboard')[0];
        var playername = document.querySelector('.endboard .playerwins .playername');
        playername.innerText = winner;
        console.log('The winner is: ');
        console.log(winner);
        endboard.style.display = 'block';
    };

    // ---------------------------- \\
    //      Private method(s)       \\
    // ---------------------------- \\

    var buildGraphics = function (self) {
        self.svg = d3.select('#multivaders').append('svg')
        .attr('id', self.person)
        .attr('class', 'playerbox')
        .attr('width', self.width + 'px')
        .attr('height', self.height + 'px');
    },
    startInvadersBombardment = function (self) {
        // var enemies = self.Enemies[self.Enemies.length-1];
        // var shooter = enemies[Math.floor(Math.random() * enemies.length)];
        var enemies;
        window.setTimeout(function () {
            self.bombardmentTimer = window.setInterval(function () {

                for (var i = 0; i < self.Enemies.length; i++) {
                    var enemyLine = self.Enemies[i];
                    if (enemyLine.length > 0) {
                        enemies = enemyLine;
                    }
                }
                // var enemies = self.Enemies[self.Enemies.length-1];
                var shooter = enemies[Math.floor(Math.random() * enemies.length)];

                if (shooter !== undefined) {
                    shooter.shoot(self);
                }
            }, self.gameSpeed/self.difficulty);
        }, self.gameSpeed/2);

    },
    startInvadersMovement = function (self) {
        console.log('----------------------------------');
        console.log('STARTING NEW INVADER MOVEMENT INTERVAL!');
        console.log('----------------------------------');
        self.gameTimer = window.setInterval(function () {
            var yDistance = 0,
            xDistance = 50 * self.direction;
            if (atEdge(self)) {
                yDistance = self.movementDistance;
                xDistance = 0;
                self.direction = self.direction * -1;
            }
            stillPlayable(self, null, self.gameTimer);
            // console.log('----------------------------------');
            // console.log('GAMESPEED: ' + self.gameSpeed);
            // console.log('----------------------------------');
            moveEnemies(self, xDistance, yDistance);
        }, self.gameSpeed);
    },
    stopInvaders = function (self) {
        clearInterval(self.gameTimer);
        clearInterval(self.bombardmentTimer);
    },
    // Check to see if the the border of the game is within the range of the next move.
    stillPlayable = function (self, Enemy, timer) {
        var distance = 50;
        var bottomLine = 0;
        if (!invadersStillAlive(self)) {
            console.log('-----------------------------');
            console.log('ALL INVADERS ARE DEAD!');
            console.log('-----------------------------');
            // clearInterval(self.gameTimer);
            // clearInterval(self.bombardmentTimer);
            self.reportEndGame(true);
            return false;
        }

        for (var i = 0; i < self.Enemies.length; i++) {
            if (self.Enemies[i].length > 0) {
                bottomLine = i;
            }
        }
        // If the invaders came to close to the bottom it's game over.
        if (self.Enemies[bottomLine][0].y >= 500) {
            console.log('CLEARING INTERVAL ** GAME ENDED');
            // clearInterval(self.gameTimer);
            // clearInterval(self.bombardmentTimer);
            self.reportEndGame(false);
            return false;
        }
    },
    atEdge = function (self) {
        var atEdge = false,
        distance = 50,
        enemy = null;

        for (var i = 0; i < self.Enemies.length; i++) {
            var EnemyLine = self.Enemies[i];

            if (EnemyLine.length <= 0 || !EnemyLine) {
                continue;
            }
            if (self.direction === DIRECTION_RIGHT) {
                enemy = EnemyLine[EnemyLine.length-1];
                if (enemy.x + enemy.width + distance > self.width) {
                    atEdge = true;
                }
            }
            else if (self.direction === DIRECTION_LEFT) {
                enemy = EnemyLine[0];
                if (enemy.x - distance < 0) {
                    atEdge = true;
                }
            }
        }
        return atEdge;
    },
    moveEnemies = function (self, xDistance, yDistance) {
        // console.log('------------------------');
        // console.log('MOVING ENEMINES');
        // console.log('------------------------');
        for (var i = 0; i < self.Enemies.length; i++) {
            var EnemyLine = self.Enemies[i];
            for (var j = 0; j < EnemyLine.length; j++) {
                var enemy = EnemyLine[j];
                enemy.move(enemy.x+xDistance, enemy.y+yDistance);
            }
        }
    },
    removeEnemy = function (self, enemy) {
        // console.log('ATTEMPTING TO REMOVE ENEMY');
        // for (var i = 0; i < self.Enemies.length; i++) {
        //     var EnemyLine = self.Enemies[i];
        //     for (var j = 0; j < EnemyLine.length; j++) {
        //         if (self.Enemies[i][j] == enemy) {
        //             self.Enemies[i].splice(j, 1);
        //             console.log('REMOVING: ' + i + '/' + j);
        //             return;
        //         }
        //     }
        // }
    },
    invadersStillAlive = function (self) {
        var status = false;
        for (var i = 0; i < self.Enemies.length; i++) {
            var EnemyLine = self.Enemies[i];
            if (EnemyLine.length > 0) {
                status = true;
            }
        }
        return status;

    };

    return GameInstance;
})();

module.exports = GameInstance;
},{"./Block":3,"./Enemy":4}],6:[function(require,module,exports){
// =============================================================== \\
// ======================== Missile Model ======================== \\
// =============================================================== \\

var Missile = (function () {
    function Missile (settings) {
        this.instance = settings.instance;
        this.server = settings.server;
        this.Blocks = this.instance.Blocks;
        this.Enemies = this.instance.Enemies;
        this.svg = settings.svg;
        this.player = settings.player;
        this.width = settings.width ? settings.width : 5;
        this.height = settings.height ? settings.height : 12;
        this.x = settings.x ? settings.x : 101;
        this.y = settings.y ? settings.y : 505;
        this.speed = settings.speed ? settings.speed : 10;
    }

    Missile.prototype.init = function () {
        buildGraphics(this);
    };

    Missile.prototype.shoot = function () {
        var _this = this,
        timer = setInterval(function () {
            moveAndCalculateCollision(timer, _this);
        }, 40);
    };


    var buildGraphics = function (self) {
        self.missile = self.svg.append('rect')
        .attr('x', self.x)
        .attr('y', self.y)
        .attr('width', self.width)
        .attr('height', self.height)
        .style('fill', '#ccc');
    },
    moveAndCalculateCollision = function (timer, self) {
        var newY = self.y - self.speed,
        missile = self.missile,
        Blocks = self.Blocks,
        Enemies = self.Enemies,
        data = {
            leftX: self.x,
            rightX: self.x + self.width,
            topY: self.y
        };
        self.y = newY;

        // If the missile has gone outside the bounds of the svg.
        if (newY <= 0) {
            clearInterval(timer);
            missile.remove();
            self.player.curMissiles = self.player.curMissiles-1;
            return;
        }

        // If the missile is around the area of the blocks.
        if (newY > 500 && Blocks.length > 0) {
            if (calculateCollision(Blocks, data)) {
                onMissileHit(self, 'block', timer);
                self.player.curMissiles = self.player.curMissiles-1;
                return;
            }
        }
        // If the missile is around the area of the player.
        if (newY > 600) {
            data.topY = self.y + self.height;
            if (self.instance.Player.checkForCollision(data)) {
                return onMissileHit(self, 'player', timer);
            }
        }
        // For everything else, it might hit the invaders.
        if (newY >= 0) {
            for (var i = Enemies.length-1; i >= 0; i--) {
                if (calculateCollision(Enemies[i], data)) {
                    onMissileHit(self, 'enemy', timer);
                    // missile.assignPointsToPlayer(1, player);
                    self.player.curMissiles = self.player.curMissiles-1;
                    return;
                }
            }
        }
        missile.transition().duration(9).ease("linear").attr('y', newY);

    },
    calculateCollision = function (items, data) {
        var ilen = items.length;
        if (ilen > 0) {
            for (var i = 0; i < ilen; i++) {
                var item = items[i];

                if (item.checkForCollision(data)) {
                    // item.destroy();
                    items.splice(i, 1);
                    return true;
                }
            }
        }
        return false;
    },
    onMissileHit = function (self, objectHit, timer) {
        self.missile.remove();
        window.clearInterval(timer);
        if (objectHit === 'enemy') {
            // self.instance.invaderDestroyed();
        }

    },
    assignPointsToPlayer = function (objectHit, player) {
        var points = 0,
        pointsField = player.get("pointsField");
        // If the player hit a block
        if (objectHit === 0) {
            points = Number(player.get("totalPoints")) + 25;
        }
        // If the player hit an enemy
        else if (objectHit === 1) {
            points = Number(player.get("totalPoints")) + 100;
        }
        player.set({totalPoints: points});
        pointsField.text(points);
 };
    return Missile;
})();

module.exports = Missile;

},{}],7:[function(require,module,exports){
var Missile = require('./Missile.js');

var Player = (function () {
    function Player (settings) {
        this.instance = settings.instance;
        this.ship = null;
        this.width = settings.width ? settings.width : 50;
        this.height = settings.height ? settings.height : 50;
        this.lives = 3;
        this.maxMissiles = 3;
        this.curMissiles = 0;
        this.svg = settings.svg ? settings.svg : null;
        this.block = settings.block ? settings.block : null;
        this.x = settings.x ? settings.x : null;
        this.y = settings.y ? settings.y : null;
    }

    Player.prototype.init = function() {
        buildGraphics(this);
    };

    Player.prototype.move = function(newX, newY) {
        if (!withinGameBorders(this, newX, newY)) {
            return;
        }
        this.x = newX;
        this.y = newY;
        this.ship.transition().duration(100)
        .attr('x', newX)
        .attr('y', newY);
    };

    Player.prototype.shoot = function(curX, curY) {
       if (this.curMissiles >= this.maxMissiles) {
           return;
       }
       this.curMissiles += 1;
       var tmpMissile = new Missile({
            instance: this.instance,
            server: null,
            player: this,
            svg: this.svg,
            x: curX+22,
            y: curY-5
       });
       tmpMissile.init();
       tmpMissile.shoot();
    };

    Player.prototype.checkForCollision = function(data) {
        // Fetch the boundary of the missile and the invader.
        var mLeftX = data.leftX,
            mRightX = data.rightX,
            mTopY = data.topY,
            leftX = this.x,
            rightX = this.x + this.width,
            topY = this.y,
            bottomY = this.y + this.height;

        // See if the missile is within the same space as the invader ship.
        var mRightXHits = (leftX < mRightX) && (mRightX < rightX),
            mLeftXHits = (leftX < mLeftX) && (mLeftX < rightX),
            mTopYHits = (topY < mTopY) && (mTopY < bottomY);

        // The invader ship is destroyed!
        if (mTopYHits && (mRightXHits || mLeftXHits)) {
            wasShot(this);
            return true;
        }
        // The invader dodged a missile(/bullet)!
        return false;
    };

    var buildGraphics = function (self) {
        // settings.ship = settings.svg.append('g')
        // .attr('transform', function () {
        //     return 'translate(0,0)';
        // });
        self.ship = self.svg.append('image')
        .attr('x', self.x)
        .attr('y', self.y)
        .attr('width', self.width)
        .attr('height', self.height)
        .attr('xlink:href', '/images/ship.png');
    },
    wasShot = function (self) {
        self.lives = self.lives - 1;
        self.ship.attr('class', 'wasShot');

        window.setTimeout(function() {
            self.ship.attr('class', '');
        }, 650);

    },
    withinGameBorders = function (self, newX, newY) {
        if ((newX+self.width) > self.instance.width) {
            return false;
        }
        else if (newX < 0) {
            return false;
        }
        else {
            return true;
        }
    };

    return Player;
})();
module.exports = Player;




},{"./Missile.js":6}]},{},[1]);