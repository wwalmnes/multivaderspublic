// =============================================================== \\
// ======================== Missile Model ======================== \\
// =============================================================== \\

var Missile = (function () {
    function Missile (settings) {
        this.instance = settings.instance;
        this.server = settings.server;
        this.Blocks = this.instance.Blocks;
        this.Enemies = this.instance.Enemies;
        this.svg = settings.svg;
        this.player = settings.player;
        this.width = settings.width ? settings.width : 5;
        this.height = settings.height ? settings.height : 12;
        this.x = settings.x ? settings.x : 101;
        this.y = settings.y ? settings.y : 505;
        this.speed = settings.speed ? settings.speed : 10;
    }

    Missile.prototype.init = function () {
        buildGraphics(this);
    };

    Missile.prototype.shoot = function () {
        var _this = this,
        timer = setInterval(function () {
            moveAndCalculateCollision(timer, _this);
        }, 40);
    };


    var buildGraphics = function (self) {
        self.missile = self.svg.append('rect')
        .attr('x', self.x)
        .attr('y', self.y)
        .attr('width', self.width)
        .attr('height', self.height)
        .style('fill', '#ccc');
    },
    moveAndCalculateCollision = function (timer, self) {
        var newY = self.y - self.speed,
        missile = self.missile,
        Blocks = self.Blocks,
        Enemies = self.Enemies,
        data = {
            leftX: self.x,
            rightX: self.x + self.width,
            topY: self.y
        };
        self.y = newY;

        // If the missile has gone outside the bounds of the svg.
        if (newY <= 0) {
            clearInterval(timer);
            missile.remove();
            self.player.curMissiles = self.player.curMissiles-1;
            return;
        }

        // If the missile is around the area of the blocks.
        if (newY > 500 && Blocks.length > 0) {
            if (calculateCollision(Blocks, data)) {
                onMissileHit(self, 'block', timer);
                self.player.curMissiles = self.player.curMissiles-1;
                return;
            }
        }
        // If the missile is around the area of the player.
        if (newY > 600) {
            data.topY = self.y + self.height;
            if (self.instance.Player.checkForCollision(data)) {
                return onMissileHit(self, 'player', timer);
            }
        }
        // For everything else, it might hit the invaders.
        if (newY >= 0) {
            for (var i = Enemies.length-1; i >= 0; i--) {
                if (calculateCollision(Enemies[i], data)) {
                    onMissileHit(self, 'enemy', timer);
                    // missile.assignPointsToPlayer(1, player);
                    self.player.curMissiles = self.player.curMissiles-1;
                    return;
                }
            }
        }
        missile.transition().duration(9).ease("linear").attr('y', newY);

    },
    calculateCollision = function (items, data) {
        var ilen = items.length;
        if (ilen > 0) {
            for (var i = 0; i < ilen; i++) {
                var item = items[i];

                if (item.checkForCollision(data)) {
                    // item.destroy();
                    items.splice(i, 1);
                    return true;
                }
            }
        }
        return false;
    },
    onMissileHit = function (self, objectHit, timer) {
        self.missile.remove();
        window.clearInterval(timer);
        if (objectHit === 'enemy') {
            // self.instance.invaderDestroyed();
        }

    },
    assignPointsToPlayer = function (objectHit, player) {
        var points = 0,
        pointsField = player.get("pointsField");
        // If the player hit a block
        if (objectHit === 0) {
            points = Number(player.get("totalPoints")) + 25;
        }
        // If the player hit an enemy
        else if (objectHit === 1) {
            points = Number(player.get("totalPoints")) + 100;
        }
        player.set({totalPoints: points});
        pointsField.text(points);
 };
    return Missile;
})();

module.exports = Missile;
