var Missile = require('./Missile.js');

var Player = (function () {
    function Player (settings) {
        this.instance = settings.instance;
        this.ship = null;
        this.width = settings.width ? settings.width : 50;
        this.height = settings.height ? settings.height : 50;
        this.lives = 3;
        this.maxMissiles = 3;
        this.curMissiles = 0;
        this.svg = settings.svg ? settings.svg : null;
        this.block = settings.block ? settings.block : null;
        this.x = settings.x ? settings.x : null;
        this.y = settings.y ? settings.y : null;
    }

    Player.prototype.init = function() {
        buildGraphics(this);
    };

    Player.prototype.move = function(newX, newY) {
        if (!withinGameBorders(this, newX, newY)) {
            return;
        }
        this.x = newX;
        this.y = newY;
        this.ship.transition().duration(100)
        .attr('x', newX)
        .attr('y', newY);
    };

    Player.prototype.shoot = function(curX, curY) {
       if (this.curMissiles >= this.maxMissiles) {
           return;
       }
       this.curMissiles += 1;
       var tmpMissile = new Missile({
            instance: this.instance,
            server: null,
            player: this,
            svg: this.svg,
            x: curX+22,
            y: curY-5
       });
       tmpMissile.init();
       tmpMissile.shoot();
    };

    Player.prototype.checkForCollision = function(data) {
        // Fetch the boundary of the missile and the invader.
        var mLeftX = data.leftX,
            mRightX = data.rightX,
            mTopY = data.topY,
            leftX = this.x,
            rightX = this.x + this.width,
            topY = this.y,
            bottomY = this.y + this.height;

        // See if the missile is within the same space as the invader ship.
        var mRightXHits = (leftX < mRightX) && (mRightX < rightX),
            mLeftXHits = (leftX < mLeftX) && (mLeftX < rightX),
            mTopYHits = (topY < mTopY) && (mTopY < bottomY);

        // The invader ship is destroyed!
        if (mTopYHits && (mRightXHits || mLeftXHits)) {
            wasShot(this);
            return true;
        }
        // The invader dodged a missile(/bullet)!
        return false;
    };

    var buildGraphics = function (self) {
        // settings.ship = settings.svg.append('g')
        // .attr('transform', function () {
        //     return 'translate(0,0)';
        // });
        self.ship = self.svg.append('image')
        .attr('x', self.x)
        .attr('y', self.y)
        .attr('width', self.width)
        .attr('height', self.height)
        .attr('xlink:href', '/images/ship.png');
    },
    wasShot = function (self) {
        self.lives = self.lives - 1;
        self.ship.attr('class', 'wasShot');

        window.setTimeout(function() {
            self.ship.attr('class', '');
        }, 650);

    },
    withinGameBorders = function (self, newX, newY) {
        if ((newX+self.width) > self.instance.width) {
            return false;
        }
        else if (newX < 0) {
            return false;
        }
        else {
            return true;
        }
    };

    return Player;
})();
module.exports = Player;



