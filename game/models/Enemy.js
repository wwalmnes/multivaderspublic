var Missile = require('./Missile.js');

var Enemy = (function () {
    function Enemy (settings) {
        this.instance = settings.instance;
        this.svg = settings.svg ? settings.svg : null;
        this.width = settings.width ? settings.width : 30;
        this.height = settings.height ? settings.height : 30;
        this.ship = settings.ship ? settings.ship : null;
        this.x = settings.x ? settings.x : 0;
        this.y = settings.y ? settings.y : 0;
        this.isDestroyed = false;
        this.imageOne = settings.imageOne ? settings.imageOne : '/images/invader1.png';
        this.imageTwo = settings.imageTwo ? settings.imageTwo : this.imageOne;
        this.worth = settings.worth ? settings.worth : 0;
        this.movementSpeed = settings.movementSpeed ? settings.movementSpeed : 0;
        this.face = 0;
    }

    // ---------------------------- \\
    //      Public method(s)        \\
    // ---------------------------- \\

    Enemy.prototype.buildGraphics = function () {
        buildGraphics(this);
    };

    Enemy.prototype.move = function (newX, newY) {
        changeImage(this);
        this.x = newX;
        this.y = newY;
        this.ship.transition().duration(this.movementSpeed)
        .attr('x', this.x)
        .attr('y', this.y);
    };

    Enemy.prototype.checkForCollision = function (data) {
        // Fetch the boundary of the missile and the invader.
        var mLeftX = data.leftX,
            mRightX = data.rightX,
            mTopY = data.topY,
            leftX = this.x,
            rightX = this.x + this.width,
            topY = this.y,
            bottomY = this.y + this.height;

        // See if the missile is within the same space as the invader ship.
        var mRightXHits = (leftX < mRightX) && (mRightX < rightX),
            mLeftXHits = (leftX < mLeftX) && (mLeftX < rightX),
            mTopYHits = (topY < mTopY) && (mTopY < bottomY);

        // The invader ship is destroyed!
        if (mTopYHits && (mRightXHits || mLeftXHits)) {
            this.isDestroyed = true;
            this.ship.remove();
            this.instance.invaderDestroyed(this);
            return true;
        }
        // The invader dodged a missile(/bullet)!
        return false;
    };

    Enemy.prototype.shoot = function(self) {
       var tmpMissile = new Missile({
            instance: self,
            server: null,
            player: this,
            svg: this.svg,
            x: this.x + (this.width/2),
            y: this.y + (this.height),
            speed: -10
       });
       tmpMissile.init();
       tmpMissile.shoot();
    };

    // ---------------------------- \\
    //      Private method(s)       \\
    // ---------------------------- \\

    var buildGraphics = function (self) {
        self.ship = self.svg.append('image')
        .attr('x', self.x)
        .attr('y', self.y)
        .attr('width', self.width)
        .attr('height', self.height)
        .attr('xlink:href', self.imageOne);
    },
    changeImage = function (self) {
        if (self.face === 0) {
            self.ship.attr('xlink:href', self.imageTwo);
            self.face = 1;
        }
        else {
            self.ship.attr('xlink:href', self.imageOne);
            self.face = 0;
        }

    };

    return Enemy;
})();

module.exports = Enemy;