

var Enemy = require('./Enemy');
var Block = require('./Block');

var DIRECTION_LEFT = -1,
    DIRECTION_RIGHT = 1;



var GameInstance = (function () {
    function GameInstance (settings) {
        this.Blocks = [];
        this.Enemies = [];
        this.Player = settings.Player ? settings.Player : null;
        this.multivaders = settings.multivaders;
        this.socket = settings.socket;

        this.direction = DIRECTION_RIGHT;
        this.width = settings.width ? settings.width : 500;
        this.height = settings.height ? settings.height : 700;
        this.movementDistance = settings.movementDistance ? settings.movementDistance : 30;
        this.difficulty = settings.difficulty ? settings.difficulty : 2;
        this.person = settings.person ? settings.person : 'me';
        this.svg = null;
        this.gameSpeed = 1500;
        this.gameTimer = null;
        this.bombardmentTimer = null;
        this.invadersDestroyedCounter = 0;
        this.invadersDestroyedMarker = [5, 10];
    }

    // ---------------------------- \\
    //      Public method(s)        \\
    // ---------------------------- \\

    GameInstance.prototype.init = function() {
        buildGraphics(this);

    };

    GameInstance.prototype.createEnemies = function (enemyNr, enemyLine, enemyAmount) {
        var enemyIndex = enemyLine-1;
        this.Enemies[enemyIndex] = this.Enemies[enemyIndex] ? this.Enemies[enemyIndex] : [];
        for (var i = 0; i < enemyAmount; i++) {
            var en = new Enemy({
                instance: this,
                svg: this.svg,
                width: 30,
                height: 30,
                x: 10 + (i*40),
                y:10 + (enemyLine * this.movementDistance),
                imageOne: '/images/invader' + enemyNr + '.png',
                imageTwo: '/images/invader' + enemyNr + '_2.png'
            });
            en.buildGraphics();
            this.Enemies[enemyIndex].push(en);
        }
    };

    GameInstance.prototype.createBlocks = function(rowAmount, colAmount, blockAmount) {
        for (var h = 0; h < blockAmount; h++) {
            for (var i = 0; i < rowAmount; i++) {
                for (var j = 0; j < colAmount; j++) {
                    var bl = new Block({svg: this.svg, x: 10 + (j*21) + (h*80), y:550 + (21*i) });
                    bl.init();
                    this.Blocks.push(bl);
                }
            }
        }
    };


    GameInstance.prototype.invaderDestroyed = function(enemy) {
        this.invadersDestroyedCounter = this.invadersDestroyedCounter + 1;
        // console.log('Destroyed this enemy: ' + enemy);
        // Remove the invader from our invader list.
        removeEnemy(this, enemy);
        // console.log('this.invadersDestroyedMarker: ' + this.invadersDestroyedMarker[0]);
        // If a certain amount of invaders have been destroyed, increase the speed.
        if (this.invadersDestroyedMarker[0] !== undefined && this.invadersDestroyedCounter >= this.invadersDestroyedMarker[0]) {
            // console.log('after: this.invadersDestroyedMarker: ' + this.invadersDestroyedMarker[0]);
            this.invadersDestroyedMarker.shift();
            this.gameSpeed = this.gameSpeed - 500;
            // console.log('SETTING GAMESPEED TO: ' + this.gameSpeed);
            window.clearInterval(this.gameTimer);
            window.clearInterval(this.bombardmentTimer);
            var self = this;
            window.setTimeout(function () {
                startInvadersMovement(self);
                startInvadersBombardment(self);
            }, 10);

            // console.log('MAKING GAME FASTER!!!!');
        }
        else if (this.invadersDestroyedMarker[0] === undefined)  {
            // console.log('InvaderDestroyed === undefined');
            // window.clearInterval(this.gameTimer);
            // window.clearInterval(this.bombardmentTimer);
        }

    };

    GameInstance.prototype.startGame = function () {
        startInvadersMovement(this);
        startInvadersBombardment(this);
    };    

    GameInstance.prototype.endGame = function (winner) {
        stopInvaders(this);
        this.showEndGameBoard(winner);
    }

    GameInstance.prototype.pauseGame = function () {
        stopInvaders(this);
    };

    GameInstance.prototype.resumeGame = function () {
        startInvadersMovement(this);
        startInvadersBombardment(this);
    };

    GameInstance.prototype.reportEndGame = function (victory) {
        stopInvaders(this);
        // Tell the server that the opponent won/lost if the player lost/won.
        this.socket.emit('endGame', { victory: !!victory });
        // Show the finished-game-view.
        // this.multivaders.showEndGameBoard(victory);
    };

    GameInstance.prototype.showEndGameBoard = function (winner) {
        var endboard = document.getElementsByClassName('endboard')[0];
        var playername = document.querySelector('.endboard .playerwins .playername');
        playername.innerText = winner;
        console.log('The winner is: ');
        console.log(winner);
        endboard.style.display = 'block';
    };

    // ---------------------------- \\
    //      Private method(s)       \\
    // ---------------------------- \\

    var buildGraphics = function (self) {
        self.svg = d3.select('#multivaders').append('svg')
        .attr('id', self.person)
        .attr('class', 'playerbox')
        .attr('width', self.width + 'px')
        .attr('height', self.height + 'px');
    },
    startInvadersBombardment = function (self) {
        // var enemies = self.Enemies[self.Enemies.length-1];
        // var shooter = enemies[Math.floor(Math.random() * enemies.length)];
        var enemies;
        window.setTimeout(function () {
            self.bombardmentTimer = window.setInterval(function () {

                for (var i = 0; i < self.Enemies.length; i++) {
                    var enemyLine = self.Enemies[i];
                    if (enemyLine.length > 0) {
                        enemies = enemyLine;
                    }
                }
                // var enemies = self.Enemies[self.Enemies.length-1];
                var shooter = enemies[Math.floor(Math.random() * enemies.length)];

                if (shooter !== undefined) {
                    shooter.shoot(self);
                }
            }, self.gameSpeed/self.difficulty);
        }, self.gameSpeed/2);

    },
    startInvadersMovement = function (self) {
        console.log('----------------------------------');
        console.log('STARTING NEW INVADER MOVEMENT INTERVAL!');
        console.log('----------------------------------');
        self.gameTimer = window.setInterval(function () {
            var yDistance = 0,
            xDistance = 50 * self.direction;
            if (atEdge(self)) {
                yDistance = self.movementDistance;
                xDistance = 0;
                self.direction = self.direction * -1;
            }
            stillPlayable(self, null, self.gameTimer);
            // console.log('----------------------------------');
            // console.log('GAMESPEED: ' + self.gameSpeed);
            // console.log('----------------------------------');
            moveEnemies(self, xDistance, yDistance);
        }, self.gameSpeed);
    },
    stopInvaders = function (self) {
        clearInterval(self.gameTimer);
        clearInterval(self.bombardmentTimer);
    },
    // Check to see if the the border of the game is within the range of the next move.
    stillPlayable = function (self, Enemy, timer) {
        var distance = 50;
        var bottomLine = 0;
        if (!invadersStillAlive(self)) {
            console.log('-----------------------------');
            console.log('ALL INVADERS ARE DEAD!');
            console.log('-----------------------------');
            // clearInterval(self.gameTimer);
            // clearInterval(self.bombardmentTimer);
            self.reportEndGame(true);
            return false;
        }

        for (var i = 0; i < self.Enemies.length; i++) {
            if (self.Enemies[i].length > 0) {
                bottomLine = i;
            }
        }
        // If the invaders came to close to the bottom it's game over.
        if (self.Enemies[bottomLine][0].y >= 500) {
            console.log('CLEARING INTERVAL ** GAME ENDED');
            // clearInterval(self.gameTimer);
            // clearInterval(self.bombardmentTimer);
            self.reportEndGame(false);
            return false;
        }
    },
    atEdge = function (self) {
        var atEdge = false,
        distance = 50,
        enemy = null;

        for (var i = 0; i < self.Enemies.length; i++) {
            var EnemyLine = self.Enemies[i];

            if (EnemyLine.length <= 0 || !EnemyLine) {
                continue;
            }
            if (self.direction === DIRECTION_RIGHT) {
                enemy = EnemyLine[EnemyLine.length-1];
                if (enemy.x + enemy.width + distance > self.width) {
                    atEdge = true;
                }
            }
            else if (self.direction === DIRECTION_LEFT) {
                enemy = EnemyLine[0];
                if (enemy.x - distance < 0) {
                    atEdge = true;
                }
            }
        }
        return atEdge;
    },
    moveEnemies = function (self, xDistance, yDistance) {
        // console.log('------------------------');
        // console.log('MOVING ENEMINES');
        // console.log('------------------------');
        for (var i = 0; i < self.Enemies.length; i++) {
            var EnemyLine = self.Enemies[i];
            for (var j = 0; j < EnemyLine.length; j++) {
                var enemy = EnemyLine[j];
                enemy.move(enemy.x+xDistance, enemy.y+yDistance);
            }
        }
    },
    removeEnemy = function (self, enemy) {
        // console.log('ATTEMPTING TO REMOVE ENEMY');
        // for (var i = 0; i < self.Enemies.length; i++) {
        //     var EnemyLine = self.Enemies[i];
        //     for (var j = 0; j < EnemyLine.length; j++) {
        //         if (self.Enemies[i][j] == enemy) {
        //             self.Enemies[i].splice(j, 1);
        //             console.log('REMOVING: ' + i + '/' + j);
        //             return;
        //         }
        //     }
        // }
    },
    invadersStillAlive = function (self) {
        var status = false;
        for (var i = 0; i < self.Enemies.length; i++) {
            var EnemyLine = self.Enemies[i];
            if (EnemyLine.length > 0) {
                status = true;
            }
        }
        return status;

    };

    return GameInstance;
})();

module.exports = GameInstance;