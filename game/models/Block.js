var Block = (function () {
    function Block (settings) {
        this.svg = settings.svg ? settings.svg : null;
        this.width = 20;
        this.height = 20;
        this.block = settings.block ? settings.block : null;
        this.x = settings.x ? settings.x : null;
        this.y = settings.y ? settings.y : null;
    }

    // ---------------------------- \\
    //      Public method(s)        \\
    // ---------------------------- \\
    Block.prototype.init = function () {
        buildGraphics(this);
    };

    // Checks if a missile is within the space of the block -> a collsion has occurred
    Block.prototype.checkForCollision = function (data) {
        // Fetch the boundary of the missile and the block.
        var mLeftX = data.leftX,
            mRightX = data.rightX,
            mTopY = data.topY,
            leftX = this.x,
            rightX = this.x + this.width,
            topY = this.y,
            bottomY = this.y + this.height;

        // See if the missile is within the same space as the block.
        var mRightXHits = (leftX < mRightX) && (mRightX < rightX),
            mLeftXHits = (leftX < mLeftX) && (mLeftX < rightX),
            mTopYHits = (topY < mTopY) && (mTopY < bottomY);

        // The block is destroyed!
        if (mTopYHits && (mRightXHits || mLeftXHits)) {
            this.block.remove();
            return true;
        }
        // The block dodged a missile(/bullet)!
        return false;
    };

    // ---------------------------- \\
    //      Private method(s)       \\
    // ---------------------------- \\
    var buildGraphics = function (self) {
        self.block = self.svg.append('rect')
        .attr('x', self.x)
        .attr('y', self.y)
        .attr('width', self.width)
        .attr('height', self.height)
        .style('fill', '#1f77b4');
    };

    return Block;
})();

module.exports = Block;