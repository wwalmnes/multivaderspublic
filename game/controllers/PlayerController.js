// =============================================================== \\
// ================== In-Game Controller Model =================== \\
// =============================================================== \\

var PlayerController = (function () {
    function PlayerController (settings) {
        this.server = settings.server;
        this.multivader = settings.multivader;
        this.player = settings.player;
        this.opponent = settings.opponent;
        this.gameActive = true;
    }


    PlayerController.prototype.init = function() {
        keyEvents(this);
        opponentEvents(this);
    };

    var keyEvents = function (self) {
        if (self.gameActive) {
            // window.onkeydown = function(event) {
            window.addEventListener('keydown', function (event) {

                var keyEvent = event.keyCode ? event.keyCode : event.which,
                    player = self.player,
                    server = self.server;
                // Left
                if (keyEvent == 37) {
                    event.preventDefault();
                    player.move((player.x-10), player.y);
                    server.emit('gameMovement', {coordX: player.x-10});
                }
                // Right
                else if (keyEvent == 39) {
                    event.preventDefault();
                    player.move((player.x+10), player.y);
                    server.emit('gameMovement', {coordX: player.x+10});
                }
                // Up (Shoot)
                else if (keyEvent == 38) {
                    event.preventDefault();
                    player.shoot(player.x, player.y);
                    server.emit('gameShooting', {coordX: player.x, coordY: player.y});
                }
            // };
            }, false);
        }
        else {
            console.log('Game is paused; keys are disabled');
        }
    };

    var opponentEvents = function (self) {

        // var controller = this;

        self.server.on('gameMovement', function(data) {
            // Spaceships can only move along the x-axis. The y-coordinate remains the same.
            self.opponent.move(data.coordX, self.opponent.y);
        });

        self.server.on('gameShooting', function(data) {
            self.opponent.shoot(data.coordX, data.coordY);
        });

        self.server.on('pauseGame', function(data) {
            // if (gameStatus === 1) { return; }
            // controller.set({ gameStatus: 1 });
            self.gameActive = false;
            // self.multivaders.pauseGame();
        });

        self.server.on('resumeGame', function(data) {
            // if (gameStatus === 0) { return; }
            // controller.set({ gameStatus: 0 });
            self.gameActive = true;
            // self.multivaders.runGame();
        });
    };

    return PlayerController;

})();

module.exports = PlayerController;

// module.exports = PlayerController;


// module.exports = Backbone.Model.extend({
//     defaults: function() {
//         return {
//             server: null,
//             multivader: null,
//             player: null,
//             opponent: null,
//             gameActive: false,
//             gameStatus: 0
//         };
//     },

//     init: function() {
//         this.keyEvents();
//         this.clickEvents();
//         this.opponentEvents();
//     },

//     keyEvents: function() {

//         var server = this.get("server");
//         var player = this.get("player");
//         var psvg = player.get("ship");
//         var controller = this;
//         d3.select("body").on('keydown', function() {
//             if (!Boolean(controller.get("gameActive"))) {
//                 console.log("Game paused: keys disabled.");
//                 return;
//             }
//             var keyEvent = d3.event.keyCode;
//             // Left
//             if (keyEvent == 65) {
//                 var curX = player.get("x");
//                 var curY = player.get("y");
//                 player.move((curX-10), curY);
//                 server.emit('gameMovement', {coordX: curX-10});
//             }
//             // Right
//             else if (keyEvent == 68) {
//                 var curX = player.get("x");
//                 var curY = player.get("y");
//                 player.move((curX+10), curY);
//                 server.emit('gameMovement', {coordX: curX-10});
//             }
//             // Up (Shoot)
//             else if (keyEvent == 87) {
//                 var curX = Number(player.get("x"));
//                 var curY = Number(player.get("y"));
//                 console.log("Up: " + curX + "/" + curY);
//                 player.shoot(curX, curY);
//                 server.emit('gameShooting', {coordX: curX, coordY: curY});
//             }
//         });
//     },

//     clickEvents: function() {
//         var server = this.get("server");
//         $('.gameButtons').on('click', '#pauseGame', function(e) {
//             e.preventDefault();
//             server.emit('changeGameStatus', { gameStatus: 'paused' });
//         });

//         $('.gameButtons').on('click', '#resumeGame', function(e) {
//             e.preventDefault();
//             server.emit('changeGameStatus', { gameStatus: 'active' });
//         });
//     },

//     opponentEvents: function() {
//         var server = this.get("server");
//         var opponent = this.get("opponent");
//         var oship = opponent.get("ship");
//         var gameStatus = this.get("gameStatus");
//         var controller = this;
//         var Multivaders = this.get("multivader");

//         server.on('gameMovement', function(data) {
//             // Spaceships can only move along the x-axis. The y-coordinate remains the same.
//             opponent.move(data.coordX, opponent.get("y"));
//         });

//         server.on('gameShooting', function(data) {
//             opponent.shoot(data.coordX, data.coordY);
//         });

//         server.on('pauseGame', function(data) {
//             if (gameStatus == 1) { return; }
//             controller.set({ gameStatus: 1 });
//             Multivaders.pauseGame();
//         });

//         server.on('resumeGame', function(data) {
//             if (gameStatus == 0) { return; }
//             controller.set({ gameStatus: 0 });
//             Multivaders.runGame();
//         });
//     }

// });