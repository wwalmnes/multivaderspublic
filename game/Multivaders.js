/** Models */
var Block = require('./models/Block'),
	Enemy = require('./models/Enemy'),
	Missile = require('./models/Missile'),
	Player = require('./models/Player'),
	GameInstance = require('./models/GameInstance'),
/** Controllers */
	PlayerController = require('./controllers/PlayerController');


var Multivaders = {

    playerInstance: null,
    opponentInstance: null,
    socket: io.connect(window.location.hostname + ':' + window.location.port),
    isHosting: false,
    isReady : false,

    init: function () {
        this.step1 = document.getElementById('step1');
        this.step2 = document.getElementById('step2');
        this.step3 = document.getElementById('step3');
        this.bodyEl = document.getElementsByTagName('body')[0];
        this.playBtn = document.getElementById('readyToPlay');
        this.pauseBtn = document.getElementById('pauseGame');
        this.resumeBtn = document.getElementById('resumeGame');
        this.usernameInputEl = document.getElementById('nameinput');
        this.hostInputEl = document.getElementById('hostinput');

        this.listenToInput();
        this.listenToSocket();

        this.socket.emit('showGames');
    },

    // Start setting up the game.
    startGame: function () {
        // Create an instance for the player and the opponent.
        var playerInstance = new GameInstance({person: 'player_me', socket: this.socket, multivaders: this}),
            opponentInstance = new GameInstance({person: 'player_opponent', socket: this.socket, multivaders: this});

        // Initiate these instances.
        playerInstance.init();
        opponentInstance.init();


        // Create a new player and opponent and give them the instance where they can play.
        var player = new Player({
            instance: playerInstance,
            svg: playerInstance.svg,
            width: 50,
            height: 25,
            x: (playerInstance.width/2)-25,
            y: playerInstance.height-40
        }),
        opponent = new Player({
            instance: opponentInstance,
            svg: opponentInstance.svg,
            width: 50,
            height: 25,
            x: (opponentInstance.width/2)-25,
            y: playerInstance.height-40
        });

        // Initiate the players.
        player.init();
        opponent.init();


        // Populate the instances.
        playerInstance.Player = player;
        playerInstance.createEnemies(1, 1, 7);
        playerInstance.createEnemies(2, 2, 7);
        playerInstance.createEnemies(3, 3, 7);
        playerInstance.createBlocks(3, 3, 6);

        opponentInstance.Player = opponent;
        opponentInstance.createEnemies(1, 1, 7);
        opponentInstance.createEnemies(2, 2, 7);
        opponentInstance.createEnemies(3, 3, 7);

        opponentInstance.createBlocks(3, 3, 6);

        // var Blocks = playerInstance.Blocks;

        // We only need one controller that is for the player. We will receive movement stuff from
        // the opponent through the server.
        var playerController = new PlayerController({server: Multivaders.socket, player: player, opponent: opponent});
        playerController.init();

        this.playerInstance = playerInstance;
        this.opponentInstance = opponentInstance;

        // Finally we can start the game :)!
        playerInstance.startGame();
        opponentInstance.startGame();
    },


    // Listens to input from the client.
    listenToInput: function () {
        Multivaders.usernameInputEl.addEventListener('keydown', Multivaders.handleInputUsername, false);
        Multivaders.hostInputEl.addEventListener('keydown', Multivaders.handleInputHost, false);
        Multivaders.resumeBtn.addEventListener('click', Multivaders.handleResume, false);
        Multivaders.pauseBtn.addEventListener('click', Multivaders.handlePause, false);
        Multivaders.playBtn.addEventListener('click', Multivaders.handlePlay, false);
        Multivaders.bodyEl.addEventListener('click', Multivaders.handleClicksOnDynamicElements, false);
    },


    // Listens to messages sent by the server.
    listenToSocket: function () {
        // Relevant to pre-game stuff. 
        Multivaders.socket.on('hostedGame', Multivaders.hostedGame);
        Multivaders.socket.on('joinGame', Multivaders.joinGame);
        Multivaders.socket.on('playGame', Multivaders.playGame);

        // Relevant to when the game has started.
        Multivaders.socket.on('endGame', Multivaders.endGame);
        Multivaders.socket.on('pauseGame', Multivaders.pauseGame);
        Multivaders.socket.on('resumeGame', Multivaders.resumeGame);
    },

    // Add the newly hosted game to the list of games.
    hostedGame: function (data) {
        var newGame = document.createElement('tr');
        newGame.innerHTML = '<td>' + data.game + '</td>' +
        '<td>' + data.player + '</td>' +
        '<td><button class="joinGame" data-game="' + data.game + '">You join now!</button></td>';

        var gamesList = document.getElementById('gameslist');
        gamesList.appendChild(newGame);
    },

    // Join a game hosted by another player.
    joinGame: function (data) {
        if (data.success) {
            // Remove the host/join games functionality
            Multivaders.step2.style.display = 'none';
            Multivaders.step3.style.display = 'block';
        }
        else {
            // Failure in joining game. Host may have dismissed game or another player joined
            // before you.
            // TODO: Provide message to inform the user about this.
        }
    },

    // The server says it's time to play!
    playGame: function () {
        var gameArea = document.getElementsByClassName('gameArea')[0];

        // Transition into the game (finally!).
        Multivaders.step3.style.display = 'none';
        gameArea.style.display = 'block';

        Multivaders.startGame();
    },

    // Received a message from the server that the game has ended.
    // That means that we've won/lost if the opponent lost/won.
    endGame: function (winner) {
        Multivaders.playerInstance.endGame(winner);
    },

    // Pause the game (for both the player and the opponent).
    pauseGame: function () {
        Multivaders.playerInstance.pauseGame();
        Multivaders.opponentInstance.pauseGame();
    },

    // Resume the game (for both the player and the opponent).
    resumeGame: function () {
        Multivaders.playerInstance.resumeGame();
        Multivaders.opponentInstance.resumeGame();
    },

    // Register the username of the player.
    handleInputUsername: function (e) {
        var keyEvent = e.keyCode ? e.keyCode : e.which;
        // If the user pressed 'enter'.
        if (keyEvent === 13) {
            Multivaders.socket.emit('joinReq', { name: this.value });
            Multivaders.hostInputEl.readOnly = false;
            Multivaders.hostInputEl.placeholder = 'you name game...';
            this.readOnly = true;

            Multivaders.step2.classList.toggle('inactive');
            Multivaders.step1.classList.toggle('inactive');
        }
    },

    // If the user wants to host a game.
    handleInputHost: function (e) {
        var keyEvent = e.keyCode ? e.keyCode : e.which;
        // If the user pressed 'enter' and is not already hosting a game.
        if (keyEvent === 13 && !Multivaders.isHosting) {
            Multivaders.socket.emit('hostGame', { gameName: this.value });
            Multivaders.isHosting = true;

            var gameMsg = document.querySelector('#step2 .gameMsg');
            gameMsg.innerText = "Lonely a little now, but friend come soon!!";
        }
    },

    // Inform the server that the user wants to resume the game.
    handleResume: function (e) {
        e.preventDefault();
        Multivaders.socket.emit('changeGameStatus', { gameStatus: 'active' });
    },

    // Inform the server that the user wants to pause the game.
    handlePause: function (e) {
        e.preventDefault();
        Multivaders.socket.emit('changeGameStatus', { gameStatus: 'paused' });
    },

    // Inform the server that you are ready to play!
    handlePlay: function (e) {
        e.preventDefault();
        if (this.isReady) {
            // TODO: Output something like "U WAIT!! FREND NOT TO READY!!"
            return;
        }
        isReady = true;
        // Tell the server that you are ready.
        Multivaders.socket.emit('awaitingReadyGame', { ready: true });
        // Inform the user that we're waiting for his "friend".
        document.querySelector('#step3 .gameMsg').innerText = 'Waiting for friend to ready!';
    },

    // When someone hosts a game a new element is added. This functions such dynamic elements.
    handleClicksOnDynamicElements: function (e) {
        if (e.target.tagName.toLowerCase() === 'button') {
            e.preventDefault();
            var target = e.target;
            if (target.classList.contains('joinGame')) {
                server.emit('joinGame', { game: target.dataset.game });
            }
        }
    }

};

// Start the application.
Multivaders.init();

