'use strict';

module.exports = function(grunt) {

    // Let's load all the grunt tasks
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    // Initialize
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        autoprefixer: {
            options: {
                browsers: ['last 2 versions', 'ie 8', 'ie 9']
            },
            dist: {
                src: 'public/stylesheets/style.less',
                dest: 'public/stylesheets/style.css'
            }
        },
        concurrent: {
            dev: {
                tasks: ['nodemon', 'watch'],
                options: {
                    logConcurrentOutput: true
                }
            }
        },
        browserify: {
            'public/javascripts/bundle.js': ['game/Multivaders.js']
        },
        uglify: {
            my_target: {
                files: {
                    'public/javascripts/bundle.min.js': ['public/javascripts/bundle.js']
                }
            }
        },
        nodemon: {
            dev: {
                script: 'bin/www',
                options: {
                    env: {
                        PORT: '3000'
                    }
                }
            }
        },
        watch: {
            scripts: {
                files: ['game/Multivaders.js', 'game/models/*.js', 'game/controllers/*.js', 'public/stylesheets/style.less'],
                tasks: ['browserify', 'uglify'],
                options: {
                    livereload: true
                }
            }
        }
    });

    // Run browserify and uglify at the start, run nodemon and watch concurrently.
    grunt.registerTask('run', ['autoprefixer', 'browserify', 'uglify', 'concurrent']);

};